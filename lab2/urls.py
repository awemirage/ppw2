from django.conf.urls.static import static
from django.conf import settings
from django.urls import path
from . import views

app_name='lab2'
urlpatterns = [
    path('', views.index, name='lab2_index'),
    path('lab2/about_me/', views.about_me, name='about_me'),
    path('lab2/contact_me/', views.contact_me, name='contact_me'),
    path('lab2/contact_me/thank_you/', views.thank_you, name='thank_you'),
    path('lab2/portfolio/', views.portfolio, name='portfolio'),
    path('lab2/portfolio/example/', views.portfolio_ex, name='portfolio_ex'),
    path('lab2/schedule/', views.schedule, name='schedule'),
    path('lab2/schedule/add_schedule/', views.add_schedule, name='add_schedule'),
    path('lab2/schedule/delete_schedule/<schedule_name>', views.delete_schedule, name='delete_schedule')
] #+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
