from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views, forms, models
from selenium import webdriver
import unittest
import time

# Create your tests here.
class Lab2UnitTest(TestCase):
    #urls
    def test_index_url_exists(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_about_me_url_exists(self):
        response = self.client.get('/lab2/about_me/')
        self.assertEqual(response.status_code, 200)
    
    def test_contact_me_url_exists(self):
        response = self.client.get('/lab2/contact_me/')
        self.assertEqual(response.status_code, 200)

    def test_thank_you_url_exists(self):
        response = self.client.get('/lab2/contact_me/thank_you/')
        self.assertEqual(response.status_code, 200)

    def test_portfolio_url_exists(self):
        response = self.client.get('/lab2/portfolio/')
        self.assertEqual(response.status_code, 200)

    def test_portfolio_ex_url_exists(self):
        response = self.client.get('/lab2/portfolio/example/')
        self.assertEqual(response.status_code, 200)

    def test_schedule_url_exists(self):
        response = self.client.get('/lab2/schedule/')
        self.assertEqual(response.status_code, 200)

    def test_add_schedule_url_exists(self):
        response = self.client.get('/lab2/schedule/add_schedule/')
        self.assertEqual(response.status_code, 200)

    def test_delete_schedule_url_exists(self):
        schedule_test = models.Schedule(name='Demo_url', date='2019-10-16', time='00:00', place='Neverwinter', category='Outting')
        schedule_test.save()
        response = self.client.get(reverse('lab2:delete_schedule', args=['Demo_url']))
        self.assertEqual(response.status_code, 200)

    #template
    def test_index_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'lab2/index.html')

    def test_about_me_template(self):
        response = self.client.get('/lab2/about_me/')
        self.assertTemplateUsed(response, 'lab2/about_me.html')
    
    def test_contact_me_template(self):
        response = self.client.get('/lab2/contact_me/')
        self.assertTemplateUsed(response, 'lab2/contact_me.html')

    def test_thank_you_template(self):
        response = self.client.get('/lab2/contact_me/thank_you/')
        self.assertTemplateUsed(response, 'lab2/thank_you.html')

    def test_portfolio_template(self):
        response = self.client.get('/lab2/portfolio/')
        self.assertTemplateUsed(response, 'lab2/portfolio.html')

    def test_portfolio_ex_template(self):
        response = self.client.get('/lab2/portfolio/example/')
        self.assertTemplateUsed(response, 'lab2/portfolio_ex.html')

    def test_schedule_template(self):
        response = self.client.get('/lab2/schedule/')
        self.assertTemplateUsed(response, 'lab2/schedule.html')

    def test_add_schedule_template(self):
        response = self.client.get('/lab2/schedule/add_schedule/')
        self.assertTemplateUsed(response, 'lab2/add_schedule.html')

    def test_delete_schedule_template(self):
        schedule_test = models.Schedule(name='Demo_url', date='2019-10-16', time='00:00', place='Neverwinter', category='Outting')
        schedule_test.save()
        response = self.client.get(reverse('lab2:delete_schedule', args=['Demo_url']))
        self.assertTemplateUsed(response, 'lab2/delete_schedule.html')

    #models
    def test_contact_model(self):
        contact_test = models.Contact(name='Timothy Wimbledon', email='Timothy@neverwinter.com', message='I am the king')
        self.assertEqual(str(contact_test), contact_test.name)

    def test_schedule_model(self):
        schedule_test = models.Schedule(name='Demo', date='2019-10-16', time='00:00', place='Neverwinter', category='Outting')
        self.assertEqual(str(schedule_test), schedule_test.name)

    #forms
    def test_add_schedule_forms(self):
        response = self.client.post('/lab2/schedule/add_schedule/', {
            'name': "Demo",
            'date': "2019-10-25",
            'time': "14:00",
            'place': "Neverwinter",
            'category': "Outting",
        })
        self.assertIn("Demo", response.content.decode())

    def test_delete_schedule_forms(self):
        schedule_test = models.Schedule(name='Demo Test', date='2019-10-16', time='00:00', place='Neverwinter', category='Outting')
        schedule_test.save()
        response = self.client.post('/lab2/schedule/', {   
            'schedule_name' : 'Demo Test'
        })
        self.assertIn("Demo Test", response.content.decode())
    
    def test_add_contact_forms(self):
        response = self.client.post('/lab2/contact_me/', {
            'name': "Timothy Wimbledon",
            'email': "Timothy@neverwinter.com",
            'message': "I am the king",
        })
        self.assertIn("Thank You for contacting us Timothy Wimbledon, you can go back to home", response.content.decode())

    
class Lab2FuctionalTest(TestCase): #Download geckodriver taro di tempat yang ada manage.py, Harus nyala bareng localhost baru bisa jalan
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab2FuctionalTest, self).setUp() 

    def tearDown(self):
        self.browser.quit()
        super(Lab2FuctionalTest, self).tearDown()

    def test_add_schedule_function(self):
        self.browser.get("http://localhost:8000/lab2/schedule/add_schedule/")
        time.sleep(2)
        tmp_input = self.browser.find_element_by_id('name')
        tmp_input.send_keys('Demo')
        time.sleep(2)
        tmp_input = self.browser.find_element_by_id('date')
        tmp_input.send_keys('2019-10-31')
        time.sleep(2)
        tmp_input = self.browser.find_element_by_id('time')
        tmp_input.send_keys('00:10')
        time.sleep(2)
        tmp_input = self.browser.find_element_by_id('place')
        tmp_input.send_keys('NeverWinter')
        time.sleep(2)
        tmp_input = self.browser.find_element_by_id('category')
        tmp_input.send_keys('Outting')
        tmp_input.submit()
        time.sleep(2)
        self.assertIn('Demo', self.browser.page_source)

# HTTP Statuses
# 2** = OK
# 3** = Movement (redirect, etc)
# 4** = User end error
# 5** = internal server error
