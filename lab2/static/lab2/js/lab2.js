var theme_is_on = false;

$(document).ready(function(){
    if(!theme_is_on){
        $(".main-theme").addClass('transparent-bg-light'); 
        $("#change_theme").text('Dark Mode');
        $("#change_theme").addClass('btn-light');
        $("body").addClass('background-light');
        theme_is_on = true;
    }
    $("#change_theme").click(function(){
        if ($(".main-theme").hasClass('transparent-bg-light')) {
            $("#change_theme").text('Light Mode');
            $("#change_theme").removeClass('btn-light');
            $("#change_theme").addClass('btn-dark');
            $(".main-theme").removeClass('transparent-bg-light');
            $(".main-theme").addClass('transparent-bg-dark');
            $("body").removeClass('background-light');
            $("body").addClass('background-dark');
        } else if ($(".main-theme").hasClass('transparent-bg-dark')) {
            $("#change_theme").text('Dark Mode');
            $("#change_theme").removeClass('btn-dark');
            $("#change_theme").addClass('btn-light');
            $(".main-theme").removeClass('transparent-bg-dark');
            $(".main-theme").addClass('transparent-bg-light');
            $("body").removeClass('background-dark');
            $("body").addClass('background-light');
        }
    });
});