from datetime import datetime, date
from django.utils import timezone
from django.db import models

# Create your models here.

class Contact(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField(max_length=30)
    message = models.CharField(max_length=300)

    def __str__(self):
        return self.name

class Schedule(models.Model):
    name = models.CharField(max_length=30)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=30)
    category = models.CharField(max_length=30)

    def __str__(self):
        return self.name