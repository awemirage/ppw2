from django import forms

class Add_Schedule(forms.Form):
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Demo',
    }
    date_attrs = {
        'type': 'date',
        'class': 'form-control',
        'id':'date',
    }
    time_attrs = {
        'type': 'time',
        'class': 'form-control',
        'id':'time',
    }
    place_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'place',
        'placeholder':'NeverWinter',
    }
    category_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'category',
        'placeholder':'Outting',
    }

    name = forms.CharField(label='Name ', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    date = forms.DateTimeField(label='Date ', required=True, widget=forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label='Time ', required=True, widget=forms.TimeInput(attrs=time_attrs))
    place = forms.CharField(label='Place ', required=True, max_length=30, widget=forms.TextInput(attrs=place_attrs))
    category = forms.CharField(label='Category ', required=True, max_length=30, widget=forms.TextInput(attrs=category_attrs))

class Add_Contact(forms.Form):
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Timothy Wimbledon',
    }
    email_attrs = {
        'type': 'email',
        'class': 'form-control',
        'id':'email',
        'placeholder':"TimothyWimbledon@neverwinter.com",
    }
    message_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'message',
        'placeholder':"Put your message here",
    }

    name = forms.CharField(label='Name ', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(label='Email ', required=True, widget=forms.TextInput(attrs=email_attrs))
    message = forms.CharField(label='Message ', required=True, max_length=300, widget=forms.Textarea(attrs=message_attrs))
    
    