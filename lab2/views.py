from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import  reverse_lazy
from datetime import datetime, date
from . import models, forms
from .forms import Add_Schedule, Add_Contact    
from .models import Contact, Schedule

# Create your views here.

def index(request):
    response = {}
    return render(request, 'lab2/index.html', response)

def about_me(request):
    response = {}
    return render(request, 'lab2/about_me.html', response)

def contact_me(request):
    contact_me_form = forms.Add_Contact(request.POST or None)
    response = {'form_contact' : contact_me_form, 'success' : ''}
    if(request.method == 'POST' and contact_me_form.is_valid()):
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['message'] = request.POST['message']
        response['success'] = "success"
        contact = models.Contact(name=response['name'], email=response['email'], message=response['message'])
        contact.save()
        contact.clean()
        return render(request, 'lab2/thank_you.html', {'name' : '' + response['name']})
    else:
        return render(request, 'lab2/contact_me.html', response)

    
def portfolio(request):
    return render(request, 'lab2/portfolio.html', {})

def thank_you(request):
    return render(request, 'lab2/thank_you.html', {})

def portfolio_ex(request):
    return render(request, 'lab2/portfolio_ex.html', {})

def schedule(request):
    schedule = models.Schedule.objects.all().values()
    response = {'list_schedule' : schedule}
    return render(request, 'lab2/schedule.html', response)

def add_schedule(request):
    schedule_form = forms.Add_Schedule(request.POST or None)
    response = {'form_schedule' : schedule_form}
    if(request.method == 'POST' and schedule_form.is_valid()):
        response['name'] = request.POST['name']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        schedule = Schedule(name=response['name'], date=response['date'], time=response['time'], place=response['place'], category=response['category'])
        print(response['date'])
        print(response['time'])
        schedule.save()
        schedule = models.Schedule.objects.all().values()
        response = {'list_schedule' : schedule}
        return render(request, 'lab2/schedule.html', response)
    else:
        return render(request, 'lab2/add_schedule.html', response)

def delete_schedule(request, schedule_name):
    schedule_1 = models.Schedule.objects.get(name = schedule_name)
    response = {'schedule_name' : str(schedule_1.name)}
    schedule_1.delete()
    return render(request, 'lab2/delete_schedule.html', response)