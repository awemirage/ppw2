"""ppw2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from . import views

app_name='lab6'
urlpatterns = [
    path('', views.index, name='lab6_index'),
    path('new_profile/', views.new_profile, name='new_profile'),
    path('search_book/', views.search_book, name='search_book'),
    path('search_book/<book_name>', views.search_book_list, name='search_book_list'),
]