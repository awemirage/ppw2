from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.http import JsonResponse
from datetime import datetime, date
from . import forms, models
import requests
import json

# Create your views here.

def index(request):
    form = forms.Add_Status(request.POST or None)
    response = {'form_status' : form}
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        response['time'] = "12:12"
        new_status = models.Status(status=response['status'])
        new_status.save()
        list_status = models.Status.objects.all().values()
        response['list_status'] = list_status
        return render(request, 'lab6/index.html', response)
    else:
        list_status = models.Status.objects.all().values()
        response['list_status'] = list_status
        return render(request, 'lab6/index.html', response)

def new_profile(request):
    return render(request, 'lab6/new_profile.html', {})

def search_book(request):
	return render(request, 'lab6/search.html')

def search_book_list(request, book_name):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=intitle:' + book_name)
    return JsonResponse(response.json())