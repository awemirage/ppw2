from django import forms

class Add_Status(forms.Form):
    status_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'status',
        'placeholder':"Job's Done",
    }

    status = forms.CharField(label='Message :', required=True, max_length=300, widget=forms.Textarea(attrs=status_attrs))