$(document).ready(function(){
	search("Lord of The Ring");

	$(function() {
		$("#accordion").accordion();
	});
});

function readInput(){
	var book_name = $('#book_name_input').val();
	$('#book_name_input').val("");
	search(book_name);
}

function search(book_name){
	$("#list_book").empty();
	$.ajax({
		url: book_name,
		success: function(list){
			if(list.totalItems != 0){
				list.items.forEach(function(book, index){
					$("#list_book").append(`
						<tr>
							<td>${index + 1}</td>
							<td><img src="${book.volumeInfo.imageLinks.thumbnail}"></td>
							<td>${book.volumeInfo.title}</td>
							<td>${book.volumeInfo.authors}</td>
							<td>${book.volumeInfo.publisher}</td>
						</tr>
					`);
				});
			} else{
				$("#list_book").append(`
					<tr>
						<td colspan=5 class="text-center">No search results found</td>
					</tr>
				`);
			}
		}
	});
}