from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views, forms, models
from selenium import webdriver
import unittest
import time

# Create your tests here.
class Lab6UnitTest(TestCase):
    #urls
    def test_index_url_exists(self):
        response = self.client.get('/lab6/')
        self.assertEqual(response.status_code, 200)

    def test_new_profile_url_exists(self):
        response = self.client.get('/lab6/new_profile/')
        self.assertEqual(response.status_code, 200)

    def test_search_book_url_exists(self):
        response = self.client.get('/lab6/search_book/')
        self.assertEqual(response.status_code, 200)

    #template
    def test_index_template(self):
        response = self.client.get('/lab6/')
        self.assertTemplateUsed(response, 'lab6/index.html')

    def test_new_profile_template(self):
        response = self.client.get('/lab6/new_profile/')
        self.assertTemplateUsed(response, 'lab6/new_profile.html')

    def test_search_book_template(self):
        response = self.client.get('/lab6/search_book/')
        self.assertTemplateUsed(response, 'lab6/search.html')

    #models
    def test_status_model(self):
        status_test = models.Status(status="Job's Done")
        self.assertEqual(str(status_test), status_test.status)

    #forms
    def test_post_status(self):
        response = self.client.post('/lab6/', {
            'status': "Job's done",
        })
        self.assertIn("Job&#39;s done", response.content.decode())

    def test_search_book_forms(self):
        response = self.client.get(reverse('lab6:search_book_list', args=['Lord of the Rings']))
        self.assertIn("The Fellowship of the Ring", response.content.decode())
        

class Lab6FuctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FuctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Lab6FuctionalTest, self).tearDown()

    def test_add_schedule_function(self):
        self.browser.get("http://localhost:8000/lab6/")
        time.sleep(2)
        tmp_input = self.browser.find_element_by_id('status')
        tmp_input.send_keys('Coba Coba')
        time.sleep(2)
        tmp_input.submit()
        time.sleep(2)
        self.assertIn('<th scope="row">Coba Coba</th>', self.browser.page_source)