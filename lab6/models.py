from datetime import datetime, date
from django.utils import timezone
from django.db import models

# Create your models here.

class Status(models.Model):
    status = models.CharField(max_length=300)
    time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.status